##Useful Spark Commands
### Accessing files in hdfs files
    data1 = sc.textFile("hdfs://cb-hdp/etl/dss/fraudoop/fraud_auth_d/svm_test/X.csv)
    data1 = data1.map(lambda row: row.split(","))
    print(data1.take(5))

### Teradata connect
    teradata_url = jdbc:teradata://nkpc1tdc/LOGMECH=LDAP,LOGDATA={username}@@{password}
    dtable = (query formated as a sub select)
    spark_dataframe = sqlContext.read.format('jdbc').options(url=tdat_url, dbtable=dtable).load()

### Save to hive table
    {your_datafram}.write.saveAsTable("database.table", format="orc", mode="overwrite")
