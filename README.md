# Ambari server installation

## I. Configure Servers *(ALL NODES)*
### 0. Configure Servers for proxy 

*update yum.conf with proxy info*

    proxy=proxy.server.address:port
    proxy_username = *******
    proxy_password = *******

*update /etc/wgetrc with proxy info (yum install if necessary)*

    https_proxy = proxy.server.address:port
    http_proxy = proxy.server.address:port
    ftp_proxy = proxy.server.address:port
    proxy_user = xxxxxx
    proxy_password = xxxxxx

*update curl proxy info as well (yum install if necessary)*

### 1. Enable and start ntpd

    ### UPDATE /etc/ntp.conf with time serves

    vi /etc/ntp.conf

    Make the following adjustment to the pool servers
    # Use public servers from the pool.ntp.org project.
    # Please consider joining the pool (http://www.pool.ntp.org/join.html).
    # server 0.rhel.pool.ntp.org iburst
    # server 1.rhel.pool.ntp.org iburst
    # server 2.rhel.pool.ntp.org iburst
    # server 3.rhel.pool.ntp.org iburst
    server {local time server1}
    server {local time server2}
    server {local time server3}

    # centos6
    chkconfig ntpd on       // starts service on reboot
    service ntpd start

    # centos7
    systemctl enable ntpd    // starts service on reboot
    systemctl start ntpd

### 2. Update hosts file
    vi /etc/hosts

*Sample File*

    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
    
    # list of hosts and ip addresses go here
    192.168.0.0 dev1.com
    192.168.0.1 dev2.com
    192.168.0.2 dev3.com
    ...
    192.168.0.n dev[n].com
### 3. Update netowrk config file

    vi /etc/sysconfig/network

*Add networking parameters*

    NETWORKING=yes
    HOSTNAME=<fully.qualified.domain.name>

### 4. Disable iptable

    # centos6
    chkconfig iptables off # service will not restart on reboot
    /etc/init.d/iptables stop

    *** 

    # centos7
    systemctl disable firewalld
    service firewalld stop

### 5. Disable selinux

    setenforce 0
    vi /etc/selinux/congig

*update SELINUX param as follows (this requires restart to take effect - will be done later)*

    SELINUX=disabled

### 6. Disable packagekit
    /etc/yum/pluginconf.d/refresh-packagekit.conf

*Update enabled flag in the file*

    enabled=0

### 7. Set unmask
    vi /etc/profile
*Add the following line at or near the end of the file*
 
    umask 022

### 8. Diable transparent_hugepage

Add the following parameter to kernal execution statements in order disable transparent hugepages


*Centos 7*

    vi /etc/default/grub

*Update the kernel parameter*

    GRUB_CMDLINE_LINUX="vconsole.font=latarcyrheb-sun16 crashkernel=auto  rd.lvm.lv=centos_dev/swap vconsole.keymap=us rd.lvm.lv=centos_dev/root rd.lvm.lv=centos_dev/usr rhgb quiet transparent_hugepage=never"

**Note transparent_hugepage=never kernel parameter (last parameter) and save file push to boot grub**
    
    grub2-mkconfig --output=/boot/grub2/grub.cfg
    
*Centos 6*

    vi /etc/grub.conf

*Update the all kernel executions with transparent_hugepage=never parameter*
    
    title Red Hat Enterprise Linux Server (2.6.32-573.7.1.el6.x86_64)
        root (hd0,0)
        kernel /vmlinuz-2.6.32-573.7.1.el6.x86_64 ro root=/dev/mapper/rootvg-rootlv rd_NO_LUKS rd_LVM_LV=rootvg/rootlv LANG=en_US.UTF-8 rd_NO_MD SYSFONT=latarcyrheb-sun16 crashkernel=auto  KEYBOARDTYPE=pc KEYTABLE=us rd_NO_DM rhgb quiet transparent_hugepage=never
        initrd /initramfs-2.6.32-573.7.1.el6.x86_64.img
    title Red Hat Enterprise Linux Server (2.6.32-504.33.2.el6.x86_64)
        root (hd0,0)
        kernel /vmlinuz-2.6.32-504.33.2.el6.x86_64 ro root=/dev/mapper/rootvg-rootlv rd_NO_LUKS rd_LVM_LV=rootvg/rootlv LANG=en_US.UTF-8 rd_NO_MD SYSFONT=latarcyrheb-sun16 crashkernel=auto  KEYBOARDTYPE=pc KEYTABLE=us rd_NO_DM rhgb quiet transparent_hugepage=never
        initrd /initramfs-2.6.32-504.33.2.el6.x86_64.img
    title Red Hat Enterprise Linux Server (2.6.32-504.12.2.el6.x86_64)
        root (hd0,0)
        kernel /vmlinuz-2.6.32-504.12.2.el6.x86_64 ro root=/dev/mapper/rootvg-rootlv rd_NO_LUKS rd_LVM_LV=rootvg/rootlv LANG=en_US.UTF-8 rd_NO_MD SYSFONT=latarcyrheb-sun16 crashkernel=auto  KEYBOARDTYPE=pc KEYTABLE=us rd_NO_DM rhgb quiet transparent_hugepage=never
        initrd /initramfs-2.6.32-504.12.2.el6.x86_64.img
    
*\*Note the transparent_hugepage=never parameter at the end of each kernal statment*

*Save file*


* * *

## II.  Download Install JDK Used By Cluster Services *(ALL NODES)*
### 1. Download oracle JDK and Java Cryptography CE Extension *(1.8.9_66 shown)*
    
*Get JDK*

    mkdir /usr/jdk64
    cd /usr/jdk64/
    wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u66-b17/jdk-8u66-linux-x64.tar.gz"
    tar -xzf jdk-8u66-linux-x64.tar.gz
    rm jdk-8u66-linux-x64.tar.gz

*Get JCE*
    
    mkdir -p jce/StrongPolicyWEAKER
    cd jce
    wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip
    unzip jce_policy-8.zip
    rm jce_policy-8.zip
        
*Move Strong Policy .jar files to StrongPolicy directory jsut created above*

    cd /usr/jdk64
    
*Move StrongPocicy out of JDK*

    mv jdk1.8.0_66/jre/lib/security/local_policy.jar jce/StrongPolicyWEAKER/
    mv jdk1.8.0_66/jre/lib/security/US_export_policy.jar jce/StrongPolicyWEAKER/
    
*Move UnlimitedPocicy into the JDK*

    mv jce/UnlimitedJCEPolicyJDK8/local_policy.jar jdk1.8.0_66/jre/lib/security/
    mv jce/UnlimitedJCEPolicyJDK8/US_export_policy.jar jdk1.8.0_66/jre/lib/security/
    ls -alh jre/lib/security  # check ownership of the new .jar files (although it appears that this is not that important)

#### Default OS to use the JDK *optional*
    alternatives --install /usr/bin/java java /usr/jdk64/jdk1.8.0_66/bin/java 2
    alternatives --config java
    ***** Choose the correct java option *****
#### Other useful alternatives to upadte 
    alternatives --install /usr/bin/jar jar /usr/jdk64/jdk1.8.0_66/bin/jar 2
    alternatives --install /usr/bin/javac javac /usr/jdk64/jdk1.8.0_66/bin/javac 2
    alternatives --set jar /usr/jdk64/jdk1.8.0_66/bin/jar
    alternatives --set javac /usr/jdk64/jdk1.8.0_66/bin/javac

* * *

## III. Install alt python *(ALL NODES)*

    yum groupinstall -y development
    yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel libpng-devel libjpg-devel atlas atlas-devel blas blas-devel lapack lapack-devel freetds freetds-devel

On prod host

    ##### CHECKING VERSION AGAINST PROD #####
    python2.7 -V
    # Note version

On installation hosts

    mkdir -p /opt/python/{VERSION}/packages
    cd /opt/python/{VERSION}/
    wget http://www.python.org/ftp/python/{VERSION}/Python-{VERSION}.tar.xz

    xz -d Python-{VERSION}.tar.xz
    tar -xf Python-{VERSION}.tar
    cd Python-{VERSION}
    ./configure --prefix=/usr/local
    make
    make altinstall
    cd /opt/python/{VERSION}/packages

on prod host

    ##### CHECKING VERSION AGAINST THOSE IN PROD #####
    pip2.7 list
    # Note setuptools and pip versions

On installation hosts


    wget --no-check-certificate {copy link for setuptools-{VERSION} from pypi.python.org} # e.g. https://pypi.python.org/packages/source/s/setuptools/setuptools-18.5.tar.gz#md5=533c868f01169a3085177dffe5e768bb
    tar -xzf setuptools-{VERSION}.tar.gz
    cd setuptools-{VERSION}
    python2.7 setup.py install
    cd /opt/python/{VERSION}/packages
    wget --no-check-certificate {copy link for pip-{VERSION} from pypi.python.org} # e.g. https://pypi.python.org/packages/source/p/pip/pip-7.1.2.tar.gz#md5=3823d2343d9f3aaab21cf9c917710196
    tar -xzf pip-{VERSION}.tar.gz
    cd pip-{VERSION}
    python2.7 setup.py install

* * *

    python dependencies still need to be managed prior to IPython setup


## IV. Install Ambari Agent *(ALL NODES)*

    yum install ambari-agent

*Set the hostname of the server in ambari-agent.ini*

    vi /etc/ambari-agent/conf/ambari-agent.ini

*Edit the hostname to be the server you will be installing the ambari-server on. double check the ports*

    [server]
    hostname=<your.ambari.server.hostname>
    url_port=8440
    secured_url_port=8441

* * *

## V. Install Ambari Server

### 1. Setup local repo if necessary

### 2. Get ambari repo *(use appropriate url if you setup local)*

    wget -v http://public-repo-1.hortonworks.com/ambari/centos7/2.x/updates/2.1.2.1/ambari.repo -O /etc/yum.repos.d/ambari.repo

*make sure ambari-{version} - Updates is in repo list*

    yum repolist 

### 3. Install the Ambari Server
    yum install ambari-server

* * *

## VI. Install MySQL *(mysql-server node only)*
### 1. Download and install MySQL repo 
    mkdir /opt/rpm
    cd /opt/rpm
    wget http://dev.mysql.com/get/mysql57-community-release-el7-7.noarch.rpm
    yum localinstall /opt/rpm/mysql57-community-release-el7-7.noarch.rpm

### 2. Enable MySQL 5.6 repo
    vi /etc/yum.repos.d/mysql-community.repo

*Change the enabled flags as shown bellow*

    [mysql56-community]
    name=MySQL 5.6 Community Server
    baseurl=http://repo.mysql.com/yum/mysql-5.6-community/el/7/$basearch/
    enabled=1
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql
    
    [mysql57-community]
    name=MySQL 5.7 Community Server
    baseurl=http://repo.mysql.com/yum/mysql-5.7-community/el/7/$basearch/
    enabled=0
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

### 3. Install mysql-connector-java
    cd /opt/
    wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.37.tar.gz
    tar -xzf mysql-connector-java-5.1.37.tar.gz
    cp mysql-connector-java-5.1.37/mysql-connector-java-5.1.37-bin.jar /usr/share/java
    cd /usr/share/java
    ln -s mysql-connector-java-5.1.37-bin.jar mysql-connector-java.jar
### 4. Start and Enable mysqld service *(syntax differnece btwen v6 and v7)*
    #centos6
    chconfig mysqld on
    service mysql start

    #centos7
    systemctl enable mysqld
    systemctl start mysqld
### 5. Configure MySQL Server for Ambari, and Hive

    mysql -u root -p

*Run setup commands (Sample script for ambari and hive with default usernames, passwords, schema)* 

#### Note the following constraints:
1. **The following assumes that you will be connecting to the mysql schema from a single instance in the cluster. Add entries as necessary**

2. **IMPORTANT -- REMEMBER *USER(s)* *PASSWORD(s)* AND *SCHEMA* AS THEY WILL BE USED LATER DURING SETUP *(default values are shown)***

3. **Only hive and ambari are shown bellow. Add Oozie and Ranger services schema as necessary**


        CREATE USER 'ambari'@'localhost' IDENTIFIED BY 'bigdata';
        GRANT ALL PRIVILEGES ON *.* TO 'ambari'@'localhost';
        CREATE USER 'ambari'@'%' IDENTIFIED BY 'bigdata';
        GRANT ALL PRIVILEGES ON *.* TO 'ambari'@'%';
        CREATE USER 'ambari'@'<fully.qualified.domain.name>'IDENTIFIED BY 'bigdata';
        GRANT ALL PRIVILEGES ON *.* TO 'ambari'@'<fully.qualified.domain.name>';
        FLUSH PRIVILEGES;
        CREATE USER 'hive'@'localhost' IDENTIFIED BY 'bigdata';
        GRANT ALL PRIVILEGES ON *.* TO 'hive'@'localhost';
        CREATE USER 'hive'@'%' IDENTIFIED BY 'bigdata';
        GRANT ALL PRIVILEGES ON *.* TO 'hive'@'%';
        CREATE USER 'hive'@'<fully.qualified.domain.name>' IDENTIFIED BY 'bigdata';
        GRANT ALL PRIVILEGES ON *.* TO 'hive'@'<fully.qualified.domain.name>';
        FLUSH PRIVILEGES;
        CREATE DATABASE hive;
        CREATE DATABASE ambari;
        USE ambari;
        SOURCE /var/lib/ambari-server/resources/Ambari-DDL-MySQL-CREATE.sql;

*If There are no errors*

    exit

* * *

## VII. Reboot server

    reboot

*Check that THP is disabled*

    cat /sys/kernel/mm/transparent_hugepage/enabled
    # Output
     > always madvise [never]

*Check that mysqld and ntpd started on restart*
    
    # centos6
    service mysqld status
    service ntpd status
    
    # centos7
    systemctl status mysqld
    systemctl status mysqld
    
* * *

## VIII. Setup Ambari Server

### 1. Run Setup Command

    ambari-server setup
    
*SAMPLE SETUP RUN **USE THE DATABASE CONFIGS SET UP IN MYSQL SETUP** *

    Using python  /usr/bin/python2.7
    Setup ambari-server
    Checking SELinux...
    SELinux status is 'disabled'
    Customize user account for ambari-server daemon [y/n] (n)? n
    Adjusting ambari-server permissions and ownership...
    Checking firewall status...
    Redirecting to /bin/systemctl status  iptables.service
    
    Checking JDK...
    [1] Oracle JDK 1.8 + Java Cryptography Extension (JCE) Policy Files 8
    [2] Oracle JDK 1.7 + Java Cryptography Extension (JCE) Policy Files 7
    [3] Custom JDK
    ==============================================================================
    Enter choice (1): 3 ** CUSTOM JDK SETUP IN STEP 1
    WARNING: JDK must be installed on all hosts and JAVA_HOME must be valid on all hosts.
    WARNING: JCE Policy files are required for configuring Kerberos security. If you plan to use Kerberos,please make sure JCE Unlimited Strength Jurisdiction Policy Files are valid on all hosts.
    Path to JAVA_HOME: /jdk64/jdk1.8.0_66/
    Validating JDK on Ambari Server...done.
    Completing setup...
    Configuring database...
    Enter advanced database configuration [y/n] (n)? y
    Configuring database...
    ==============================================================================
    Choose one of the following options:
    [1] - PostgreSQL (Embedded)
    [2] - Oracle
    [3] - MySQL
    [4] - PostgreSQL
    [5] - Microsoft SQL Server (Tech Preview)
    [6] - SQL Anywhere
    ==============================================================================
    Enter choice (1): 3 (custom jdk installed during server setup)
    Hostname (localhost): <fully.qualified.domain.name> (set your value here)
    Port (3306): (use default)
    Database name (ambari): (USE YOUR AMBARI DATABASE CREATED DURING MYSQL SETUP)
    Username (ambari): (USE YOUR AMBARI USERNAME CREATED DURING MYSQL SETUP)
    Enter Database Password (bigdata): (USE YOUR AMBARI USERNAME'S PASSWORD CREATED DURING MYSQL SETUP)
    Configuring ambari database...
    Copying JDBC drivers to server resources...
    Configuring remote database connection properties...
    WARNING: Before starting Ambari Server, you must run the following DDL against the database to create the schema: /var/lib/ambari-server/resources/Ambari-DDL-MySQL-CREATE.sql
    Proceed with configuring remote database connection properties [y/n] (y)? y
    Extracting system views...
    ambari-admin-2.1.2.1.418.jar
    ......
    Adjusting ambari-server permissions and ownership...
    Ambari Server 'setup' completed successfully.

### 2. Register the mysql ambari databese and mysql-connector-java
    
    ambari-server setup --jdbc-db mysql --jdbc-driver /usr/share/java/mysql-connector-java-5.1.37-bin.jar

* * *

## IX. Run the ambari server and all agents

*On ambari-server node*

    ambari-server start
    
*On all nodes*
    
    ambari-agent start

***Optional** - If all goes well during start up enable ambari-server and enable each of the ambari-agent service so they start on reboot

*On the ambari-server host*

    #centos6
    chkconfig ambari-server on

    #cnetos7
    systemctl enable ambari-server

*On the ALL hosts*

    #centos6
    chkconfig ambari-agent on

    #cnetos7
    systemctl enable ambari-agent
* * *

## X. Run Yarn config script for start point for service config {DEPRICATED USE SMARTSENSE}

*Use [hdp-configuration-utils.py] (https://github.com/hortonworks/hdp-configuration-utils/blob/master/2.1/hdp-configuration-utils.py "Hortonworks config util"). (It is a little dated but a good start)*

    python hdp-configuration-utils.py -c 12 -m 15 -d 1 -k False
    
    #####
    -c = # of cores 
    -m = GB of Memory 
    -d = # of disks
    -k = hbaseEnabled

## XI. Configure Ambari to authenticate against AD