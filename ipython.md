# IPython Notbook setup

### Login to your cluster server

Move into your root/home directory

    cd ~

Edit your ~/.profile file

    vi .profile

Add the following lines bellow the Comment #Add personal stuff here

press 'i' to use insert mode

    # Hortonworks ready gloabl path var
    PATH=usr/lib64/qt-3.3/bin:/opt/boksm/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/hdp/current/falcon-client/bin:/usr/hdp/current/hadoop-mapreduce-historyserver/bin:/usr/hdp/current/oozie-client/bin:/usr/hdp/current/falcon-server/bin:/usr/hdp/current/hadoop-yarn-client/bin:/usr/hdp/current/oozie-server/bin:/usr/hdp/current/flume-client/bin:/usr/hdp/current/hadoop-yarn-nodemanager/bin:/usr/hdp/current/pig-client/bin:/usr/hdp/current/flume-server/bin:/usr/hdp/current/hadoop-yarn-resourcemanager/bin:/usr/hdp/current/slider-client/bin:/usr/hdp/current/hadoop-client/bin:/usr/hdp/current/hadoop-yarn-timelineserver/bin:/usr/hdp/current/sqoop-client/bin:/usr/hdp/current/hadoop-hdfs-client/bin:/usr/hdp/current/hbase-client/bin:/usr/hdp/current/sqoop-server/bin:/usr/hdp/current/hadoop-hdfs-datanode/bin:/usr/hdp/current/hbase-master/bin:/usr/hdp/current/storm-client/bin:/usr/hdp/current/hadoop-hdfs-journalnode/bin:/usr/hdp/current/hbase-regionserver/bin:/usr/hdp/current/storm-nimbus/bin:/usr/hdp/current/hadoop-hdfs-namenode/bin:/usr/hdp/current/hive-client/bin:/usr/hdp/current/storm-supervisor/bin:/usr/hdp/current/hadoop-hdfs-nfs3/bin:/usr/hdp/current/hive-metastore/bin:/usr/hdp/current/zookeeper-client/bin:/usr/hdp/current/hadoop-hdfs-portmap/bin:/usr/hdp/current/hive-server2/bin:/usr/hdp/current/zookeeper-server/bin:/usr/hdp/current/hadoop-hdfs-secondarynamenode/bin:/usr/hdp/current/hive-webhcat/bin:/usr/hdp/current/hadoop-mapreduce-client/bin:/usr/hdp/current/knox-server/bin:/usr/hdp/current/hadoop-client/sbin:/usr/hdp/current/hadoop-hdfs-nfs3/sbin:/usr/hdp/current/hadoop-yarn-client/sbin:/usr/hdp/current/hadoop-hdfs-client/sbin:/usr/hdp/current/hadoop-hdfs-portmap/sbin:/usr/hdp/current/hadoop-yarn-nodemanager/sbin:/usr/hdp/current/hadoop-hdfs-datanode/sbin:/usr/hdp/current/hadoop-hdfs-secondarynamenode/sbin:/usr/hdp/current/hadoop-yarn-resourcemanager/sbin:/usr/hdp/current/hadoop-hdfs-journalnode/sbin:/usr/hdp/current/hadoop-mapreduce-client/sbin:/usr/hdp/current/hadoop-yarn-timelineserver/sbin:/usr/hdp/current/hadoop-hdfs-namenode/sbin:/usr/hdp/current/hadoop-mapreduce-historyserver/sbin:/usr/hdp/current/hive-webhcat/sbin:/root/bin
    export PATH

press 'esc' then 'wq' to save and quit

### Generate the default config file
    jupyter notebook --generate-config

### Create hashed password from a python terminal
    $ python2.7
You should see somehting like:

    Python 2.7.11 (default, Dec 18 2015, 15:49:56)
    [GCC 4.4.7 20120313 (Red Hat 4.4.7-16)] on linux2
    Type "help", "copyright", "credits" or "license" for more information.
    >>>

Know create your hashed password for your IPython notebook password (example)

    >>>: from notebook.auth import passwd
    >>>: passwd()
    Enter password:
    Verify password:
    u'sha1:67c9e60bb8b6:9ffede08654sadl254b2e042ea597d771089e11aed'

### Now edit the jupyter notebook config file at:
    vi ~/.jupyter/jupyter_notebook_config.py

### Edit the following lines appropriately

press 'i' to use insert mode

    67:     c.NotebookApp.ip = {use the hostname of the machine you are on}
    88:     c.NotebookApp.port = {GET THIS FROM JOHN}
    102:    c.NotebookApp.allow_origin = '*'
    137:    c.NotebookApp.notebook_dir = u'dev/jupyter_notebooks'
    177:    c.NotebookApp.open_browser = False
    186:    c.NotebookApp.password = {use the password you just generated}

press 'esc' then 'wq' to save and quit

## Create the appropriate directories on your machine
    mkdir ~/dev
    mkdir ~/dev/jupyter_notebooks

### If you are moving your files from another machine
#### Login to the machine that you currently contains your jupyter notebooks
    scp /local/path/to/jupyter_notebooks_dir/* {username}@{new_host):~/dev/jupyter_notebooks

### Run the notebook
    $ pyspark --queue {AD user id}